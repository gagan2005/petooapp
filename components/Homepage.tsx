import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  Divider,
  FAB,
} from 'react-native-paper';
import React from 'react';
import {View, Text, StyleSheet, ImageBackground, Image} from 'react-native';
const mainurl =
  'https://d39in59pr3ya79.cloudfront.net/p/img/Home-Page-Banner_02.jpg';
const logourl = 'https://d39in59pr3ya79.cloudfront.net/p/img/logo.png';
export default class Homepage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <ImageBackground
          style={{flex: 1, flexDirection: 'column'}}
          source={{
            uri: mainurl,
          }}>
            <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}/>
            <Image source={{uri: logourl}} style={{flex:1.3}} />
            <View style={{flex:1}}/>
            </View>
          <View style={{flex: 7}}>
            
          </View>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1}} />
            <FAB
              style={styles.fab}
              label="View Menu"
              icon="menu"
              onPress={this.props.next}
            />
            <View style={{flex: 1}} />
          </View>
        </ImageBackground>
      </>
    );
  }
}

const styles = StyleSheet.create({
  fab: {
    flex: 2,
    marginBottom: 25,
    backgroundColor: 'blue',
  },
});
