import React, { Component } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
} from "react-native";
import { FAB, Appbar, Colors,ActivityIndicator } from "react-native-paper";
import Itemcard from "./Card";
import Description from "./Description";

var data = [];

export default class Menu extends Component {
  data = [];
  constructor(props: any) {
    super(props);
   
    this.state = { isloaded: false,modalvisible:false,modaldes:" " };
    this.showModal=this.showModal.bind(this);
    this.closeModal=this.closeModal.bind(this);
  }
  showModal(obj)
  {
    this.setState({modalvisible:true,modaldes:obj['description']});
  }
  closeModal()
  {
    this.setState({modalvisible:false,modaldes:" "});
  }
  componentDidMount()
  {
    this.getdata();
  }

  getdata() {
    // var data=[];
    var url =
      "https://jsonblob.com/api/jsonBlob/4fc2f1cd-4d80-11ea-8962-c1dcfb86c24b";
    fetch(url)
      .then((response) => response.json())
      .then((res) => {
        console.log(res);
        var arr: Itemcard[] = [];
        i=0;
        res.forEach((ele) => {
          const temp = <Itemcard info={ele} do={this.showModal} key={i} />;
          console.log("temo", temp);
          arr.push(temp);
          i++;
        });
        console.log(data);
        this.setState({ data2: arr, isloaded: true });
      });
    // return data;
  }
  render() {
    if (this.state.isloaded == true)
      return (
        <View>
          <ScrollView>{this.state.data2}</ScrollView>
          <Description visible={this.state.modalvisible} desc={this.state.modaldes} close={this.closeModal}/>
        </View>
      );
    else return <ActivityIndicator  style={styles.loader} animating={true} size="large" />
  }
}

const styles=StyleSheet.create(
  {
    loader:{
      margin:'auto',
      flex:1
    }
  }
)