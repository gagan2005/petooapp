import React from 'react';
import Homepage from './Homepage';
import Menu from "./menu";

export default class Main extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={page:1};
        this.next=this.next.bind(this);
    }
    next()
    {
        this.setState({page:2});
    }
    render()
    {
        if(this.state.page==1)
        {
            return <Homepage next={this.next}/>;
        }
        else
        {
            return <Menu/>
        }
    }
}