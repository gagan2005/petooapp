import { Avatar, Button, Card, Title, Paragraph, Divider } from 'react-native-paper';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native'
import { green200 } from 'react-native-paper/lib/typescript/src/styles/colors';

export default class Itemcard extends React.Component {
    constructor(props: any) {
        super(props);
        this.display = this.display.bind(this);
    }
    display() {
        console.log("pressed");
        this.props.do(this.props.info);

    }
    render() {
        return (<View style={{ padding: 12 }}>
            <Card elevation={2} onPress={this.display} >
                <Card.Title title={this.props.info['family_name']} />
                <Card.Content>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 3, flexDirection: "column" }}>
                            <Text>Category - {this.props.info['category_name']}</Text>
                            <Vegbut isveg={this.props.info['is_veg']} />
                        </View>
                        <View style={styles.price}>
                            <Text style={styles.price}>₹{this.props.info['price']}</Text>
                        </View>


                    </View>
                </Card.Content>

            </Card>
        </View>
        );
    }
}
function Vegbut(props: any) {
    if (props.isveg === '1')
        return (<View style={[styles.round, styles.g]}>

        </View>);
    else {
        return (<View style={[styles.round, styles.r]}>

        </View>);
    }
}

const styles = StyleSheet.create({
    round: {
        width: 15,
        height: 15,
        borderRadius: 7.5,
        margin: 4
    },
    g: {
        backgroundColor: "green",

    },
    r: {
        backgroundColor: "red",

    },
    price:
    {
        flex: 1,
        fontSize: 20
    }
});
