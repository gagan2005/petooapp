import * as React from 'react';
import { Card,Modal,Portal, Text, Button, Provider } from 'react-native-paper';
import { View } from 'react-native';

export default class Description extends React.Component {
  
    constructor(props:any){
        super(props);
    this.state = {
    visible: false,
  };
  this.closeModal=this.closeModal.bind(this);
}

  closeModal()
  {
      this.props.close();
  }
  showModal(disp:string)
  {
      this.setState({visible:true,desc:disp})
  }

  render() {
    return (
      <Provider>
         <Portal>
           <Modal visible={this.props.visible} onDismiss={this.closeModal} animationType="fade" presentationStyle="pageSheet">
             <Card style={{margin:8,padding:8}}>
                 <Text style={{fontSize:20}}>{this.props.desc}</Text>
                 </Card>
           </Modal>
         </Portal>
      </Provider>
    );
  }
}