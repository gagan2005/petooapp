# Petoo #

A react-native app

### Demo ###
Screenshots are available in the 'screenshots folder'

To view the demo use the app-debug.apk

### Build Instructions ###

* Make sure you have React-native CLI up and running.Follow React-Native CLI quickstart from 'https://reactnative.dev/docs/environment-setup'
* Run npm install to install the required dependencies.
* Connect your android device and run 

   npm react-native run-android

